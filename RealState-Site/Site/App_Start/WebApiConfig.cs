﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Site
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                //routeTemplate: "api/Mobile/{controller}/{id}",                                
                //defaults: new { id = RouteParameter.Optional }                
                routeTemplate: "api/{controller}/{id}",                                                
                defaults: new { area = "Mobile", controller = "Product", id = RouteParameter.Optional }   
            );
            config.Routes.MapHttpRoute(
               name: "Api_GET",
               routeTemplate: "api/{controller}/{id}",
               constraints: new HttpMethodConstraint(HttpMethod.Get),
               defaults: new
               {
                   area = "Mobile",
                   controller = "Product",
                   id = RouteParameter.Optional
               }
           );
            config.Routes.MapHttpRoute(
               name: "Api_POST",
               routeTemplate: "api/{controller}/{id}",
               constraints: new HttpMethodConstraint(HttpMethod.Post),
               defaults: new
               {
                   area = "Mobile",
                   controller = "Product"
               }
           );

            config.Routes.MapHttpRoute(
               name: "Api_Action",
               routeTemplate: "api/{controller}/{action}/{id}",
               defaults: new
               {
                   area = "Mobile",
                   controller = "Product",
                   action = "Hello",
                   id = RouteParameter.Optional
               }
           ); 
        }
    }
}
