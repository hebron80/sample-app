﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using IoC.Core.Migrations;
using IoC.Core.ServiceManagement;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Site.Areas.Mobile;
using System.Net.Http.Formatting;

namespace Site
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                                 
            WebApiConfig.Register(GlobalConfiguration.Configuration);            
            GlobalConfiguration.Configuration.EnsureInitialized();            

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings.Add(
                new RequestHeaderMapping("Accept", "text/html", 
                    StringComparison.InvariantCultureIgnoreCase, 
                    true, "application/json"));

            if (System.Data.Entity.Database.Exists("MyRealStateDBContext"))
            {
                return;
            }
            System.Data.Entity.Database.SetInitializer(new System.Data.Entity.MigrateDatabaseToLatestVersion<RealStateDBContext, Configuration>());                     
        }              
    }
}
