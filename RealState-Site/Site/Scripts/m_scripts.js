﻿$(document).ready(function () {
     
    //$('.row .page-number').click(function (e) {
    $('#product_related .pagination-container .pagination li a').click(function () {
        
        //get current page
        var curPage = parseInt($(this).html());

        //get id
        var id = function () {
            return document.location.href.split('/').pop();
        }
        $.ajax({
            type: 'POST',
            //url: '@Url.Action("ProductRelated", "Product")',                                    
            url:'ProductRelated',
            data: { id:id,page: curPage },
            dataType: 'html',
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",                        
            success: function (data) {
                alert(data);
                $('div div[class="row"]').last().html(data);
            },
            error: function (e) {
                console.error('Error' + e);               
            }
        });
    });
});