﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;

namespace Site.Areas.Customer.Models
{
    public class ProductOwnerViewModel
    {
        public Int64 ProductOwnerViewModelId { get; set; }       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public ICollection<Product> Products { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}