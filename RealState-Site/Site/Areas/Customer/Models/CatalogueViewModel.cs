﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IoC.Core.Entities;

namespace Site.Areas.Customer.Models
{
    public class CatalogueViewModel
    {
        
        public Int64 CatalogueId {get;set;}
        public string Name{get;set;}
        public string Description{get;set;}



    }
}