﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;
using IoC.Core.ServiceManagement;
using IoC.Core.ServiceManagement.ManagerCatalogues;

namespace Site.Areas.Customer.Models
{
    public class HomeViewModel
    {

        public IList<Catalogues> Catalogues { get; set; }
        public IList<SlideHome> SlideHomes { get; set; }


        public HomeViewModel() { }
        public HomeViewModel(IList<Catalogues> _CataLogues, IList<SlideHome> _SlideHome) 
        {
            this.Catalogues = _CataLogues;
            this.SlideHomes = _SlideHome;
        }

    }
}