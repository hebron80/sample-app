﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;

namespace Site.Areas.Customer.Models
{
    public class FileStoreViewModel
    {
        public Int64 FileId { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public Int64 ProductId { get; set; }

        public FileStoreViewModel() { }

        public FileStoreViewModel(FileStore fs) 
        {
            this.FileId = fs.FileId;
            this.FileName = fs.FileName;
            this.Description = fs.Description;            
        }
    }
}