﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IoC.Core.ServiceManagement.ManagerUser;
using Site.Areas.Customer.Models;

namespace Site.Areas.Customer.Controllers
{
    public class UserController : Controller
    {
        private IUserService _userService;


        public UserController(IUserService userService)
        {
            this._userService = userService;
        }


        //
        // GET: /User/
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult ItemList()
        {
            IEnumerable<UserViewModel> userModel = _userService.GetUsers().Select(s => new UserViewModel 
            {
                FirstName = s.FirstName,
                LastName = s.LastName,
                AccountName = s.AccountName,
                Password = s.Password,
                DateCreated = s.DateCreated,
                DateUpdated = s.DateUpdated
            });
            return View(userModel);
        }
	}
}