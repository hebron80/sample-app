﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using IoC.Core.ServiceManagement.ManagerProduct;
using IoC.Core.ServiceManagement.ManagerFileStore;
using IoC.Core.ServiceManagement.ManagerProductOwner;
using IoC.Core.ServiceManagement.ManagerCatalogues;
using IoC.Core.Entities;
using Site.Areas.Customer.Models;
using PagedList;
using IoC.Core.Pagination;

namespace Site.Areas.Customer.Controllers
{
    public class ProductController : Controller
    {
        //private RealStateDBContext db = new RealStateDBContext();
        private IProductService _productService;
        private IFileStoreService _filestoreService;
        private IProductOwnerService _productownerService;
        private ICatalogueService _catalogueService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="productService"></param>

        public ProductController(ICatalogueService catalogueService, IProductService productService, IFileStoreService filestoreService, IProductOwnerService productownerService)
        {
            this._productService = productService;
            this._filestoreService = filestoreService;
            this._productownerService = productownerService;
            this._catalogueService = catalogueService;
        }

        // GET: /Product/
        public ActionResult Index(long id, int? page)
        {                        
            List<ProductViewModel> list = new List<ProductViewModel>();            
            var products = _productService.GetProducts();
            
            foreach(var item in products)
            {
                ProductViewModel pvm = new ProductViewModel(item);                                              
                list.Add(pvm);
            }

            //paging
            int pagesize = 5;
            int nopage = (page ?? 1);
            return View(list.OrderBy(i => i.Name).ToPagedList(nopage, pagesize));
        }
        
        //GET: /ProductDetail/1
        public ActionResult ProductDetail(long id)
        {            
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);            

            var product = _productService.GetOneProductWithMoreInfo(id);            
            if (product == null)
            {
                return HttpNotFound();
            }
            ProductViewModel productModel = new ProductViewModel(product);            
            return View(productModel);
        }
         
        public ActionResult ProductList(long id, int? page)
        {
            
            List<ProductViewModel> list = new List<ProductViewModel>();            
            var products = _productService.GetProductsByCataloguesID(id);
            
            foreach (var item in products)
            {
                ProductViewModel pvm = new ProductViewModel(item);
                list.Add(pvm);
            }

            if (list.Count() == 0)
            {
                var catalogueName = _catalogueService.GetCatalogue(id);
                CommonViewModel commonVM = new CommonViewModel();
                commonVM.CatalogueName = catalogueName.Name;
                return View("ProductListNoContent", commonVM);
            }

            //paging
            int pagesize = 5;
            int nopage = (page ?? 1);
            return View(list.OrderBy(i=>i.Name).ToPagedList(nopage,pagesize));
        }

      
        
        public ActionResult ProductRelated(long id, int? page)
        {
            List<ProductViewModel> list = new List<ProductViewModel>();
            //var products = _productService.GetProducts().Take(300);
            var products = _productService.GetProductsByCataloguesID(id);
            if (products == null) return View("Index");

            foreach (var item in products)
            {
                ProductViewModel pvm = new ProductViewModel(item);
                list.Add(pvm);
            }

            if(list.Count() == 0)
            {
                return View("NoContent");
            }
            
            //paging           
            int pagesize = 3;
            return PartialView(new PaginatedList<ProductViewModel>(list, page ?? 1, pagesize));            
            //return View(list.OrderBy(i => i.Name).ToPagedList(nopage, pagesize));
        }
       
        //POST: Upload multip files
        [HttpPost]
        
        public ActionResult UploadMultipFiles(IEnumerable<HttpPostedFileBase> files,ProductViewModel model)
        {
            //Product p = new Product();
            //List<FileStore> listFile = new List<FileStore>();
            //FileStore filestore = new FileStore();
            
            ////p.FileStores = _filestoreService.GetAllFileStoreByProduct(model.ProductId);
            //p = _productService.GetProduct(model.ProductId);

            //foreach (var file in files)
            //{
            //    if (file.ContentLength > 0 && file != null)
            //    {
            //        string imgUrl = Path.Combine(Server.MapPath("~/App_Data/Upload/Customer/"),
            //            Path.GetFileName(Guid.NewGuid() + Path.GetExtension(file.FileName)));
            //        file.SaveAs(imgUrl);                  
            //        filestore.FileName = file.ToString();
            //        listFile.Add(filestore);
            //    }
            //}
            //p.FileStores = listFile;
            //_productService.Update(p);

            return RedirectToAction("Index");
        }

        // GET: /Product/Details/5
        public ActionResult Details(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = _productService.GetProduct(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: /Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,Name,Hight,Width,Direction,Floors,WC,BedRoom,BathRoom,EAH,Image1,Image2,Image3,Image4,Image5,Description,FK_CataloguesId,FK_ProductOwnerId,Id,DateCreated,DateUpdated")] Product productviewmodel)
        {
            if (ModelState.IsValid)
            {
                _productService.Insert(productviewmodel);                
                return RedirectToAction("Index");
            }

            return View(productviewmodel);
        }

        // GET: /Product/Edit/5
        public ActionResult Edit(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            Product product = _productService.GetProduct(id);

            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: /Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        //public ActionResult EditPost([Bind(Include = "ProductId,Name,Hight,Width,Direction,Floors,WC,BedRoom,BathRoom,EAH,Image1,Image2,Image3,Image4,Image5,Description,FK_CataloguesId,FK_ProductOwnerId,Id,DateCreated,DateUpdated")] Product productviewmodel)        
        public ActionResult EditPost(Product productviewmodel)        
        {
            Product p = new Product();
            if (ModelState.IsValid)
            {
                p = _productService.GetProduct(productviewmodel.ProductId);                
                p.Name = productviewmodel.Name;
                p.Direction = productviewmodel.Direction;
                _productService.Update(p);                
                return RedirectToAction("Index");
            }
            return View(productviewmodel);
        }

        // GET: /Product/Delete/5
        public ActionResult Delete(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product productviewmodel = _productService.GetProduct(id);
            if (productviewmodel == null)
            {
                return HttpNotFound();
            }
            return View(productviewmodel);
        }

        // POST: /Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {    
            Product productviewmodel = _productService.GetProduct(id);
            _productService.Delete(productviewmodel);
            return RedirectToAction("Index");
            
        }

        protected override void Dispose(bool disposing)
        {
            //if (disposing)
            //{
            //    db.Dispose();
            //}
            base.Dispose(disposing);
        }
    }
}
