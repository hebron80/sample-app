﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IoC.Core.Entities;
using IoC.Core.ServiceManagement.ManagerProductOwner;
using PagedList;
using Site.Areas.Customer.Models;
namespace Site.Areas.Customer.Controllers
{
    public class ProductOwnerController : BaseController
    {
        

        //
        // GET: /ProductOwner/

        private IProductOwnerService _productownerService;

        public ProductOwnerController(IProductOwnerService productownerService)
        {
            this._productownerService = productownerService;
        }
      
        public ActionResult Index(int page = 1, int pageSize = 3)
        {
            IEnumerable<ProductOwnerViewModel> listProductOwner = _productownerService.GetProductOwners().Select(p => new ProductOwnerViewModel
            {
                
                FirstName = p.FirstName,
                LastName = p.LastName,
                FullName = p.FirstName + " " + p.LastName
            });
            
            var model = new PagedList<ProductOwnerViewModel>(listProductOwner.ToList(), page, pageSize);
            //var model = listProductOwner.OrderBy(i => i.FullName).ToPagedList(page, pageSize);
            return View(model);
        }
	}
}