﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using IoC.Core.ServiceManagement.ManagerProduct;
using IoC.Core.ServiceManagement.ManagerFileStore;
using IoC.Core.ServiceManagement.ManagerProductOwner;
using IoC.Core.Entities;
using Site.Areas.Customer.Models;
using PagedList;
using MvcSiteMapProvider;

namespace Site.Areas.Customer.Controllers
{
    public class HiController : Controller
    {
        //
        // GET: /Customer/Hi/

        private IProductService _productService;
        private IFileStoreService _filestoreService;
        private IProductOwnerService _productownerService;

        public HiController()
        { }

        public HiController(IProductService productService, IFileStoreService filestoreService, IProductOwnerService productownerService)
        {
            this._productService = productService;
            this._filestoreService = filestoreService;
            this._productownerService = productownerService;
        }


        public ActionResult Index()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult List(long Id, int? page)
        {

            List<ProductViewModel> list = new List<ProductViewModel>();
            var products = _productService.GetProductsByCataloguesID(Id);

            foreach (var item in products)
            {
                ProductViewModel pvm = new ProductViewModel(item);
                list.Add(pvm);
            }

            //paging
            int pagesize = 5;
            int nopage = (page ?? 1);
            return View(list.OrderBy(i => i.Name).ToPagedList(nopage, pagesize));
        }
	}
}