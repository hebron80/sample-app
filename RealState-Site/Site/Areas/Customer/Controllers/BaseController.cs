﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Site.Areas.Customer.Models;


namespace Site.Areas.Customer.Controllers   
{
    //[Authorize]
    public class BaseController : Controller
    {
        
        //
        // GET: /Base/
        public UserManager<ApplicationUser> UserManager { get; set; }
        public BaseController(): this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {

        }
        public BaseController(UserManager<ApplicationUser> userManager)
        {
            this.UserManager = userManager;
        }
       
	}
}