﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IoC.Core.ServiceManagement.ManagerCatalogues;
using Site.Areas.Customer.Models;
using PagedList;


namespace Site.Areas.Customer.Controllers
{
    public class CatalogueController : Controller
    {
        private ICatalogueService _catalogueService;

        public CatalogueController(ICatalogueService catalogueService )
        {
            this._catalogueService = catalogueService;
        }


        //
        // GET: /Catalogue/
        public ActionResult Index()
        {
            IEnumerable<CatalogueViewModel> list = _catalogueService.GetCatalogues().Select(c => new CatalogueViewModel
            { 
                Name = c.Name,
                Description = c.Description
            });
            
            return View(list.ToList());            
        }

        public ActionResult CatalogueList()
        {
            IEnumerable<CatalogueViewModel> list = _catalogueService.GetCatalogues().Select(c => new CatalogueViewModel
            {
                CatalogueId = c.CataloguesId,
                Name = c.Name,
                Description = c.Description
            });
            if(list.Count() == 0)
            {
                return View("NoContent");
            }
            return View(list);
            
        }


        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
	}
}