﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IoC.Core.Entities;
using IoC.Core.ServiceManagement.ManagerSlideHome;
using IoC.Core.ServiceManagement.ManagerCatalogues;
using Site.Areas.Customer.Models;

namespace Site.Areas.Customer.Controllers
{
    public class HomeController : Controller
    {
        private ICatalogueService _catalogueService;
        private ISlideHomeService _slidehomeService;

        public HomeController(ICatalogueService catalogueService, ISlideHomeService slidehomeService)
        {
            this._catalogueService = catalogueService;
            this._slidehomeService = slidehomeService;
        }

        public ActionResult Index()
        {
            var cataList = _catalogueService.GetThreeCatalogues();
            var slideList = _slidehomeService.GetSlideHomes();
            if (cataList.Count() == 0)
            {
                return View("NoContent");
            }
            HomeViewModel hv = new HomeViewModel(cataList,slideList);
            return View(hv);
            
        }

        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}