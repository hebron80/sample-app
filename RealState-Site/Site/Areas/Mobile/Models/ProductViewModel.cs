﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IoC.Core.Entities;

namespace Site.Areas.Mobile.Models
{
    public class ProductViewModel
    {
        public Int64 ProductId { get; set; }
        public decimal Price { get; set; }
        public string Name { get; set; }
        public float Hight { get; set; }
        public float Width { get; set; }
        public string Direction { get; set; }   //East, West, South, North of the Production   
        public int Floors { get; set; }
        private string Status { get; set; } //So hong, so do, giay tay        
        public int WC { get; set; }
        public int BedRoom { get; set; }
        public int BathRoom { get; set; }
        public string EAH { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string Description { get; set; }
        //public string CataloguesName { get; set; }
        //public Int64 FK_CataloguesId { get; set; }
        //public Int64 FK_ProductOwnerId { get; set; }
        //public ProductOwner ProductOwner { get; set; }
        //public ICollection<FileStore> FileStores { get; set; }

        //public ProductViewModel() { }
        //public ProductViewModel(Product p)
        //{
        //    this.ProductId = p.ProductId;
        //    this.Name = p.Name;
        //    this.Price = System.Math.Round(p.Price / 1000, 2);
        //    this.Floors = p.Floors;
        //    this.Hight = p.Hight;
        //    this.Width = p.Width;
        //    this.Status = p.Status;
        //    this.WC = p.WC;
        //    this.FileStores = p.FileStores;
        //    this.ProductOwner = p.ProductOwner;
        //    this.Direction = p.Direction;
        //    this.BathRoom = p.BathRoom;
        //    this.BedRoom = p.BedRoom;
        //    this.Description = p.Direction;
        //    this.CataloguesName = p.Catalogues.Name;
        //}
    }
}