﻿using System.Web.Mvc;
using System.Web.Http;


namespace Site.Areas.Mobile
{
    public class MobileAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Mobile";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Mobile_default",
                "Mobile/{controller}/{action}/{id}",
                new { controller = "Product", id = UrlParameter.Optional },
                namespaces: new[] { "Site.Areas.Mobile.Controllers" }
            );      
        }
    }
}