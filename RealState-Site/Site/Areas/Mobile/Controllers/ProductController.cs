﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IoC.Core.ServiceManagement.ManagerProduct;
using IoC.Core.ServiceManagement.ManagerFileStore;
using IoC.Core.ServiceManagement.ManagerProductOwner;
using IoC.Core.ServiceManagement.ManagerCatalogues;
using IoC.Core.Entities;
using Site.Areas.Mobile.Models;
using PagedList;
using IoC.Core.Pagination;

namespace Site.Areas.Mobile.Controllers
{
    public class ProductController : ApiController
    {
        //private IProductService _productService;
        //private IFileStoreService _filestoreService;
        //private IProductOwnerService _productownerService;
        //private ICatalogueService _catalogueService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="productService"></param>

        //public ProductController(ICatalogueService catalogueService, IProductService productService, IFileStoreService filestoreService, IProductOwnerService productownerService)
        //{
        //    this._productService = productService;
        //    this._filestoreService = filestoreService;
        //    this._productownerService = productownerService;
        //    this._catalogueService = catalogueService;
        //}

        // GET: /Product/   
        
        //public Product Get(long id)
        //{            
        //    var product = _productService.GetProduct(id);
        //    if (product == null)
        //    {
        //        return null;
        //    }
        //    //ProductViewModel productModel = new ProductViewModel(product);
        //    return product;
            
        //}
        //[HttpGet]
        //public Product GetHello()
        //{
        //    Product product = new Product();
        //    product.Name = "Nhà Riêng";
        //    return product;
        //}
        //public Product GetItem()
        //{
        //    Product model = new Product();
        //    model.Name = "Nhà Riêng";
        //    model.BedRoom = 5;
        //    model.BathRoom = 3;
        //    model.WC = 3;
        //    model.Width = 5;
        //    model.Hight = 20;
        //    model.Price = 15848;            
        //    model.EAH = "Khu dân trí cao, an ninh, thuận tiện trường, chợ, bệnh viên, phường, quận";
        //    model.Direction = "Đông Nam";

        //    return model;
        //}
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public ProductViewModel GetItem()
        {
            ProductViewModel model = new ProductViewModel();
            model.ProductId = 5;
            model.Name = "Nhà Riêng";
            model.BedRoom = 5;
            model.BathRoom = 3;
            model.WC = 3;
            model.Width = 5;
            model.Hight = 20;
            model.Price = 15848;            
            model.EAH = "Khu dân trí cao, an ninh, thuận tiện trường, chợ, bệnh viên, phường, quận";
            model.Direction = "Đông Nam";

            return model;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public string Hello()
        {
            return "Hello, This is a product";
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public string Hello(string name)
        {
            return "Hello, " + name + " This is a product";
        }
    }
}
