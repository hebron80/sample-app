﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Collections.Generic;
using IoC.Core.Entities;


namespace IoC.Core.Migrations
{
  

    //internal sealed class Configuration : DbMigrationsConfiguration<IoC.Core.ServiceManagement.RealStateDBContext>    
    public sealed class Configuration : DbMigrationsConfiguration<IoC.Core.ServiceManagement.RealStateDBContext>    
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            
        }

        protected override void Seed(IoC.Core.ServiceManagement.RealStateDBContext context)        
        {

            var catalogues = new List<Catalogues> 
            { 
                new Catalogues{CataloguesId = 1, Name="Căn Hộ Cao Cấp", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 2, Name="Nhà Mặt Phố", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 3, Name="Nhà Chung Cư", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 4, Name="Biệt Thự Cao Cấp", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 5, Name="Khu Nghỉ Dưỡng", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 6, Name="Nhà Xưởng, Kho", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 7, Name="Trang Trại", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 8, Name="Đất Nền Dự Án", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 9, Name="Cửa Hàng KIOT", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 10, Name="Văn Phòng", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Catalogues{CataloguesId = 11, Name="Nhà Trọ, Phòng Trọ", Description ="Chúng tôi luôn làm việc để đem đến cho bạn nhiều sự lựa chọn và phù hợp nhất từ những nhà môi giới chuyên nghiệp và uy tín. Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")}
            };
            catalogues.ForEach(c => context.Catalogues.Add(c));
            context.SaveChanges();

            var productowner = new List<ProductOwner> 
            { 
                new ProductOwner{ProductOwnerId = 1, FirstName="Tín", LastName="Diệp", Address="123 Truong Dinh", Avatar="",Email="tin@gmail.com", Mobile="0989654785", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new ProductOwner{ProductOwnerId = 2, FirstName="Huy", LastName="Lê", Address="34 Tran Hung Dao", Avatar="",Email="huy@gmail.com", Mobile="0989654785", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new ProductOwner{ProductOwnerId = 3, FirstName="Thành", LastName="Hứa", Address="56 Truong Quoc Dung", Avatar="",Email="thanh@gmail.com", Mobile="0989654785", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new ProductOwner{ProductOwnerId = 4, FirstName="Linh", LastName="Phan", Address="190 Nguyen Trong Tuyen", Avatar="",Email="linh@gmail.com", Mobile="0989654785", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new ProductOwner{ProductOwnerId = 5, FirstName="Triết", LastName="Nguyễn", Address="200 Nguyen Du", Avatar="",Email="triet@gmail.com", Mobile="0989654785", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new ProductOwner{ProductOwnerId = 6, FirstName="Lộc", LastName="Đoàn", Address="200 Lê Văn Khương", Avatar="",Email="loc@gmail.com", Mobile="0989654785", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")}
            };
            productowner.ForEach(c => context.ProductOwner.Add(c));
            context.SaveChanges();
            
            var products = new List<Product> 
            { 
                new Product{ProductId = 1, Name ="Căn Hộ Thơi An City", Price=750, Hight=45, Width=5, Direction="Đông Bắc", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="TA 15",Ward="Thới An", District="12", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=1, FK_Product_ProductOwnerId= 1, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 2, Name ="Căn Hộ VinPearl Q.1 HCM", Price = 1500, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="TA 16",Ward="Thới An", District="12", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=1, FK_Product_ProductOwnerId= 1,DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 3, Name ="Căn Hộ VinPearl Q.1 HCM", Price = 2500, Hight=45, Width=5, Direction="Đông Tây", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Mai Thị Lựu",Ward="Hiệp Thành", District="11", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=1, FK_Product_ProductOwnerId= 2, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 4, Name ="Căn Hộ VinPearl Q.1 HCM", Price = 3500, Hight=45, Width=5, Direction="Đông Tây", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Thoại Ngọc Hầu",Ward="Hiệp Thành", District="11", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=2,FK_Product_ProductOwnerId= 2, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 5, Name ="Căn Hộ VinPearl Q.1 HCM", Price = 5500, Hight=45, Width=5, Direction="Đông Tây", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Lê Lợi",Ward="Tân Chánh Hiệp", District="1", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=2,FK_Product_ProductOwnerId= 3,  DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 6, Name ="Căn Hộ VinPearl Q.1 HCM", Price = 2500, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Lê Thánh Tôn",Ward="Tân Chánh Hiệp", District="1", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=3,FK_Product_ProductOwnerId= 3,  DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 7, Name ="Căn Hộ VinPearl Q.1 HCM", Price = 1500, Hight=45, Width=5, Direction="Đông Bắc", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Trần Hưng Đạo",Ward="Đông Hưng Thuận", District="10", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=3,FK_Product_ProductOwnerId= 4,  DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 8, Name ="Căn Hộ VinPearl Q.1 HCM", Price = 6500, Hight=45, Width=5, Direction="Đông Bắc", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Trần Văn Kiểu",Ward="Đông Hưng Thuận", District="10", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=4, FK_Product_ProductOwnerId= 4, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 9, Name ="Căn Hộ VinPearl Q.1 HCM", Price = 7500, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Trần Quốc Thảo",Ward="Thạnh Xuân", District="3", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=4,FK_Product_ProductOwnerId= 5,  DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 10, Name ="Căn Hộ Himlam Q.8 HCM", Price = 7500, Hight=45, Width=5, Direction="Tây Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Trần Văn Ơn",Ward="Thạnh Xuân", District="3", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=5,FK_Product_ProductOwnerId= 5,  DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 11, Name ="Căn Hộ Himlam Q.8 HCM", Price = 1200, Hight=45, Width=5, Direction="Tây Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="ĐL Võ Văn Kiệt",Ward="Tân Hưng Thuận", District="5", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=5, FK_Product_ProductOwnerId= 6, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 12, Name ="Căn Hộ Himlam Q.8 HCM", Price = 1000, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Trần Cao Vân",Ward="Tân Hưng Thuận", District="5", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=6,FK_Product_ProductOwnerId= 6,  DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 13, Name ="Căn Hộ Himlam Q.8 HCM", Price = 1700, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Võ Thị Sáu",Ward="An Phú Đông", District="6", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=6, FK_Product_ProductOwnerId= 6, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 14, Name ="Căn Hộ Himlam Q.8 HCM", Price = 2800, Hight=45, Width=5, Direction="Tây Bắc", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Châu Văn Liêm",Ward="An Phú Đông", District="6", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=7, FK_Product_ProductOwnerId= 6, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 15, Name ="Căn Hộ Himlam Q.8 HCM", Price = 3570, Hight=45, Width=5, Direction="Tây Bắc", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Lương Đình Của",Ward="Thạnh Lộc", District="7", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=7, FK_Product_ProductOwnerId= 6, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 16, Name ="Căn Hộ Himlam Q.8 HCM", Price = 5580, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Cao Văn Lầu",Ward="Thạnh Lộc", District="7", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=8, FK_Product_ProductOwnerId= 6, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 17, Name ="Căn Hộ Gardent View Q.12 HCM", Price = 3590, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, Address="Đặng Quốc Dung",Ward="Tân Thới Nhất", District="8", LocatedMap="",Video1="",Video2="",Video3="", WC=5, Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=8, FK_Product_ProductOwnerId= 6, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 18, Name ="Căn Hộ Gardent View Q.12 HCM", Price = 5580, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, Address="Phạm Thế Hiển",Ward="Tân Thới Nhất", District="8", LocatedMap="",Video1="",Video2="",Video3="", WC=5, Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=9, FK_Product_ProductOwnerId= 6, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 19, Name ="Căn Hộ Gardent View Q.12 HCM", Price = 8590, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Phó Đức Chính",Ward="Trung My Tây", District="2", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=9,FK_Product_ProductOwnerId= 6,  DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 20, Name ="Căn Hộ Gardent View Q.12 HCM", Price = 2580, Hight=45, Width=5, Direction="Đông Bắc", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Tạ Quang Bửu",Ward="Trung My Tây", District="2", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=10,FK_Product_ProductOwnerId= 6,  DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 21, Name ="Căn Hộ Gardent View Q.12 HCM", Price = 4560, Hight=45, Width=5, Direction="Đông Nam", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Trần Bình Trọng",Ward="Thới An", District="2", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=11, FK_Product_ProductOwnerId= 6, DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")},
                new Product{ProductId = 22, Name ="Căn Hộ Gardent View HCM",Price = 2580, Hight=45, Width=5, Direction="Tây Bắc", BathRoom=3, BedRoom=5, EAH="Tốt", Floors=5, WC=5, Address="Lưu Trọng Lu",Ward="Thới An", District="2", LocatedMap="",Video1="",Video2="",Video3="", Description="Để có thể biết thêm thông tin về Bất động sản này cũng như tiến hành giao kết, mời bạn liên hệ với chuyên viên môi giới theo form dưới đây", FK_Product_CataloguesId=11,FK_Product_ProductOwnerId= 6,  DateCreated = DateTime.Parse("2013-05-25"), DateUpdated = DateTime.Parse("2013-05-25")}
            };
            products.ForEach(p => context.Product.Add(p));
            context.SaveChanges();

            var filestore = new List<FileStore>
            {
                new FileStore{ FileId = 1, FileName = "~/Images/Upload/Customer/57087141695_phong-khach.jpg",FK_FileStore_ProductId = 1,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 2, FileName = "~/Images/Upload/Customer/giuongnguKS.jpg",FK_FileStore_ProductId = 2,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 3, FileName = "~/Images/Upload/Customer/13552123123.jpg",FK_FileStore_ProductId = 3,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 4, FileName = "~/Images/Upload/Customer/i9_1.jpg",FK_FileStore_ProductId = 4,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 5, FileName = "~/Images/Upload/Customer/thiet-ke-noi-that-can-ho-penthouse-2.jpg",FK_FileStore_ProductId = 5,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},

                new FileStore{ FileId = 6, FileName = "~/Images/Upload/Customer/57087141695_phong-khach.jpg",FK_FileStore_ProductId = 6,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 7, FileName = "~/Images/Upload/Customer/giuongnguKS.jpg",FK_FileStore_ProductId = 7,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 8, FileName = "~/Images/Upload/Customer/13552123123.jpg",FK_FileStore_ProductId = 8,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 9, FileName = "~/Images/Upload/Customer/i9_1.jpg",FK_FileStore_ProductId = 9,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 10, FileName = "~/Images/Upload/Customer/thiet-ke-noi-that-can-ho-penthouse-2.jpg",FK_FileStore_ProductId = 10,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},

                new FileStore{ FileId = 11, FileName = "~/Images/Upload/Customer/57087141695_phong-khach.jpg",FK_FileStore_ProductId = 11,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 12, FileName = "~/Images/Upload/Customer/giuongnguKS.jpg",FK_FileStore_ProductId = 12,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 13, FileName = "~/Images/Upload/Customer/13552123123.jpg",FK_FileStore_ProductId = 13,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 14, FileName = "~/Images/Upload/Customer/i9_1.jpg",FK_FileStore_ProductId = 14,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 15, FileName = "~/Images/Upload/Customer/thiet-ke-noi-that-can-ho-penthouse-2.jpg",FK_FileStore_ProductId = 15,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},

                new FileStore{ FileId = 16, FileName = "~/Images/Upload/Customer/57087141695_phong-khach.jpg",FK_FileStore_ProductId = 16,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 17, FileName = "~/Images/Upload/Customer/giuongnguKS.jpg",FK_FileStore_ProductId = 17,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 18, FileName = "~/Images/Upload/Customer/13552123123.jpg",FK_FileStore_ProductId = 18,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 19, FileName = "~/Images/Upload/Customer/i9_1.jpg",FK_FileStore_ProductId = 19,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 20, FileName = "~/Images/Upload/Customer/thiet-ke-noi-that-can-ho-penthouse-2.jpg",FK_FileStore_ProductId = 20,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 21, FileName = "~/Images/Upload/Customer/thiet-ke-noi-that-can-ho-penthouse-2.jpg",FK_FileStore_ProductId = 21,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new FileStore{ FileId = 22, FileName = "~/Images/Upload/Customer/thiet-ke-noi-that-can-ho-penthouse-2.jpg",FK_FileStore_ProductId = 22,DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")}
            };
            filestore.ForEach(c => context.FileStore.Add(c));
            context.SaveChanges();

            var role = new List<Role> 
            {
                new Role{ RoleId=1,Name="administrator", Description="Administrator", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Role{ RoleId=2,Name="suppervisor", Description="Suppervisor", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Role{ RoleId=3,Name="user", Description="User", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new Role{ RoleId=4,Name="moderator", Description="Moderator", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")}

            };
            role.ForEach(c => context.Role.Add(c));
            context.SaveChanges();

            var user = new List<User> 
            {
                new User{UserId=1, FirstName="Hưng", LastName="Đoàn", AccountName="htd001", Password="123",  DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new User{UserId=2, FirstName="Lộc", LastName="Đoàn", AccountName="ltd001", Password="123",  DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new User{UserId=3, FirstName="Phú", LastName="Đoàn", AccountName="ptd001", Password="123",  DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")}

            };
            user.ForEach(c => context.User.Add(c));
            context.SaveChanges();

            var slidehome = new List<SlideHome>
            {
                new SlideHome{ImageId = 1, Image = "~/Images/SlideHome/slide-1.jpg",DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new SlideHome{ImageId = 2, Image = "~/Images/SlideHome/slide-2.jpg", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")},
                new SlideHome{ImageId = 3, Image = "~/Images/SlideHome/slide-3.jpg", DateCreated=DateTime.Parse("2013-05-26"),DateUpdated=DateTime.Parse("2013-05-21")}

            };
            slidehome.ForEach(c => context.SlideHome.Add(c));
            context.SaveChanges();          
        }
    }
}
