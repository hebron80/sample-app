﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using IoC.Core.Entities;


namespace IoC.Core.ServiceManagement
{
    public interface IService<T> where T: BaseEntity
    {
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);       
    }
}
