﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement
{
    public interface IRepository<T> where T:BaseEntity
    {
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
        T GetById(object Id);
        IQueryable<T> Table { get; }

    }
}
