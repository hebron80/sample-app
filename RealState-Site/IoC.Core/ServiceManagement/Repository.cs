﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Validation;
using IoC.Core.Entities;


namespace IoC.Core.ServiceManagement
{
    public class Repository<T>:IRepository<T> where T:BaseEntity
    {
        private readonly IDbContext _context;
        private IDbSet<T> _entities;

        public Repository(IDbContext context)
        {
            this._context = context;
        }

        public T GetById(object Id)
        {
            return this.Entities.Find(Id);
        }

        public void Insert(T entity)
        {
            try 
            {
                if(entity == null)
                {
                    throw new ArgumentNullException("Entity");
                }
                this.Entities.Add(entity);
                this._context.SaveChanges();
            }
            catch(DbEntityValidationException ex)
            {
                var msg = string.Empty;
                foreach(var validationErrors in ex.EntityValidationErrors)
                { 
                    foreach(var validationError in validationErrors.ValidationErrors)
                    {                        
                        msg += string.Format("Property: {0}, Error: {1}",validationError.PropertyName,validationError.ErrorMessage);
                    }
                }
            }
        }
        public void Delete(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("Entity");
                }
                this.Entities.Remove(entity);
                this._context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        msg += string.Format("Property: {0}, Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
        }
        public void Update(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("Entity");
                }
                
                this._context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        msg += string.Format("Property: {0}, Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
        }
        public virtual IQueryable<T> Table
        {
            get 
            { 
                return this.Entities;
            }
        }
        public IDbSet<T> Entities
        {
            get 
            {
                if(_entities == null)
                {
                    _entities = _context.Set<T>();
                }
                return _entities;
            }
        }


    }
}