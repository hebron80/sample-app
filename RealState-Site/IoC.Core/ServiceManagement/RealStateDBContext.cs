﻿using System;
using System.Reflection;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.ModelConfiguration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement
{
    public class RealStateDBContext:DbContext,IDbContext
    {
        public IDbSet<Catalogues> Catalogues { get; set; }
        public IDbSet<Product> Product { get; set; }
        public IDbSet<ProductOwner> ProductOwner { get; set; }
        public IDbSet<FileStore> FileStore { get; set; }
        public IDbSet<User> User { get; set; }
        public IDbSet<Role> Role { get; set; }
        public IDbSet<SlideHome> SlideHome { get; set; }
        
      
        public RealStateDBContext() : base("name=MyRealStateDBContext") 
        {               
        }
               
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Catalogues>().
                HasMany(p => p.Products).WithRequired(i => i.Catalogues).HasForeignKey(f => f.FK_Product_CataloguesId);

            //modelBuilder.Entity<Role>().
            //    HasMany(u => u.Users).WithRequired(i => i.Role).HasForeignKey(f => f.FK_RoleId);

            //modelBuilder.Entity<Product>().
            //    HasMany(p => p.ProductOwners).WithMany(i => i.Products).
            //    Map(m =>
            //    {
            //        m.MapLeftKey("ProductId");
            //        m.MapRightKey("ProductOwnerId");
            //    });

            modelBuilder.Entity<Product>().
                HasMany(p => p.FileStores).WithRequired(i => i.Product).
                HasForeignKey(f => f.FK_FileStore_ProductId);

            modelBuilder.Entity<ProductOwner>().
                HasMany(p => p.Products).WithRequired(i => i.ProductOwner).
                HasForeignKey(f=>f.FK_Product_ProductOwnerId);
                
                
               

            //modelBuilder.Entity<Catalogues>().ToTable("Catalogues");
            //modelBuilder.Entity<Product>().ToTable("Product");
            //modelBuilder.Entity<ProductOwner>().ToTable("ProductOwner");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<SlideHome>().ToTable("SlideHome");

        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }
        public new int SaveChanges()
        {             
            return base.SaveChanges();
        }        
    }
}