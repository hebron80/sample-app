﻿using System.Data.Entity;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement
{
    public interface IDbContext
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;
        int SaveChanges();

    }
}
