﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;


namespace IoC.Core.ServiceManagement.ManagerProductOwner
{
    public class ProductOwnerService : IProductOwnerService
    {
        private IRepository<ProductOwner> _productownerRepo;
        public ProductOwnerService(IRepository<ProductOwner> productownerRepo)
        {
            this._productownerRepo = productownerRepo;
        }

        public IQueryable<ProductOwner> GetProductOwners()
        {
            return _productownerRepo.Table;
        }
        public ProductOwner GetProductOwnerByID(long ProductOwnerId)
        {
            return _productownerRepo.GetById(ProductOwnerId);
        }

        public ProductOwner GetProductOwnerByProductID(long ProductId)
        {
            var po = from p in _productownerRepo.Table
                              where p.Products.Any(c=>c.ProductId == ProductId)                         
                              select p;
            return po.FirstOrDefault();
        }

        public void Insert(ProductOwner productowner)
        {
            _productownerRepo.Insert(productowner);
        }
        public void Update(ProductOwner productowner)
        {
            _productownerRepo.Update(productowner);
        }
        public void Delete(ProductOwner productowner)
        {
            _productownerRepo.Delete(productowner);
        }


    }
}