﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement.ManagerProductOwner
{
    public interface IProductOwnerService : IService<ProductOwner>
    {
        IQueryable<ProductOwner> GetProductOwners();
        ProductOwner GetProductOwnerByID(long ProductOwnerId);
        ProductOwner GetProductOwnerByProductID(long ProductId);

        
    }
}
