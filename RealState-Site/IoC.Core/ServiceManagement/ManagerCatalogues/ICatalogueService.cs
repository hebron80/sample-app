﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement.ManagerCatalogues
{
    public interface ICatalogueService:IService<Catalogues>
    {
        IList<Catalogues> GetCatalogues();
        Catalogues GetCatalogue(long Id);
        IList<Catalogues> GetThreeCatalogues();
        
    }
}
