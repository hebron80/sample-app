﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement.ManagerCatalogues
{
    public class CatalogueService:ICatalogueService
    {
        private IRepository<Catalogues> _catalogueRepo;

        public CatalogueService(IRepository<Catalogues> catalogueRepo)
        {
            this._catalogueRepo = catalogueRepo;
        }

        public IList<Catalogues> GetCatalogues()
        {
            return _catalogueRepo.Table.ToList();
        }
        public IList<Catalogues> GetThreeCatalogues()
        {
            var list = from c in _catalogueRepo.Table.Take(3)
                       select c;
            return list.ToList();
        }
        public Catalogues GetCatalogue(long Id)
        {
            return _catalogueRepo.GetById(Id);
        }


        public void Insert(Catalogues catalogue)
        {
            _catalogueRepo.Insert(catalogue);
        }
        public void Update(Catalogues catalogue)
        {
            _catalogueRepo.Update(catalogue);
        }
        public void Delete(Catalogues catalogue)
        {
            _catalogueRepo.Delete(catalogue);
        }


    }
}