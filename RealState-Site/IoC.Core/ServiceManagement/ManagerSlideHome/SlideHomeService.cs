﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;


namespace IoC.Core.ServiceManagement.ManagerSlideHome
{
    public class SlideHomeService:ISlideHomeService
    {
        private readonly IRepository<SlideHome> _slidehomeRepo;

        public SlideHomeService(IRepository<SlideHome> slidehomeRepo)
        {
            this._slidehomeRepo = slidehomeRepo;
        }

        public IList<SlideHome> GetSlideHomes()
        {
            return _slidehomeRepo.Table.ToList();
        }

        public SlideHome GetSlideHome(long SlideHomeId)
        {
            return _slidehomeRepo.GetById(SlideHomeId);
        }

        public void Insert(SlideHome slidehome)
        {
            _slidehomeRepo.Insert(slidehome);
        }
        public void Delete(SlideHome slidehome)
        {
            _slidehomeRepo.Delete(slidehome);
        }
        public void Update(SlideHome slidehome)
        {
            _slidehomeRepo.Update(slidehome);
        }

    }
}