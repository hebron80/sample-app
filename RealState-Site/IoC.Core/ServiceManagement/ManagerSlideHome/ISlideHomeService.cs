﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement.ManagerSlideHome
{
    public interface ISlideHomeService:IService<SlideHome>
    {
        IList<SlideHome> GetSlideHomes();        
        SlideHome GetSlideHome(long SlideHomeId);
    }
}
