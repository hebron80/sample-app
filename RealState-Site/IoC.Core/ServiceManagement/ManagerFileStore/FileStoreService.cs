﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement.ManagerFileStore
{
    public class FileStoreService:IFileStoreService
    {
        private IRepository<FileStore> _filestoreRepo;

        public FileStoreService(IRepository<FileStore> filestoreRepo)
        {
            this._filestoreRepo = filestoreRepo;
        }

        public IEnumerable<FileStore> GetFileStores()
        {
            return _filestoreRepo.Table;
        }

        public FileStore GetFileStore(long FileId)
        {
            return _filestoreRepo.GetById(FileId);
        }
        public IEnumerable<FileStore> GetAllFileStoreByProduct(long ProductId)
        {
            var list = from c in _filestoreRepo.Table
                       where c.FK_FileStore_ProductId == ProductId
                       orderby c.FileName
                       select c;
            
            if (list == null) return null;
            return list.ToList();
        }       

        public void Insert(FileStore filestore)
        {
            _filestoreRepo.Insert(filestore);
        }
        public void Update(FileStore filestore)
        {
            _filestoreRepo.Update(filestore);
        }
        public void Delete(FileStore filestore)
        {
            _filestoreRepo.Delete(filestore);
        }
    }
}