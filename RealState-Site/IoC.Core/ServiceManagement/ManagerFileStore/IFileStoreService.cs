﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement.ManagerFileStore
{
    public interface IFileStoreService:IService<FileStore>
    {
        IEnumerable<FileStore> GetFileStores();
        FileStore GetFileStore(long FileId);
        IEnumerable<FileStore> GetAllFileStoreByProduct(long ProductId);        
    }
}
