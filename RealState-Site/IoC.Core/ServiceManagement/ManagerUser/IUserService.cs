﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IoC.Core.Entities;


namespace IoC.Core.ServiceManagement.ManagerUser
{
    public interface IUserService:IService<User>
    {      
        IQueryable<User> GetUsers();
        User GetUser(long Id);
    }
    
}
