﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;


namespace IoC.Core.ServiceManagement.ManagerUser
{
    public class UserService:IUserService
    {
        private IRepository<User> _userRepo;

        public UserService(IRepository<User> userRepo)
        {
            _userRepo = userRepo;
        }

        public IQueryable<User> GetUsers()
        {
            return _userRepo.Table;
        }

        public User GetUser(long Id)
        {
            return _userRepo.GetById(Id);
        }
        public void Insert(User user)
        {
            _userRepo.Insert(user);
        }
        public void Delete(User user)
        {
            _userRepo.Delete(user);
        }
        public void Update(User user)
        {
            _userRepo.Update(user);
        }
    }
}