﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IoC.Core.Entities;


namespace IoC.Core.ServiceManagement.ManagerProduct
{
    public class ProductService:IProductService
    {
        private IRepository<Product> _productRepo;
        private IRepository<Catalogues> _cataloguesRepo;
        private IRepository<FileStore> _filestoreRepo;
        private IRepository<ProductOwner> _productownerRepo;

        
        public ProductService(IRepository<Product> productRepo,IRepository<Catalogues> cataloguesRepo, 
            IRepository<FileStore> filestoreRepo, IRepository<ProductOwner> productownerRepo)
        {
            this._productRepo = productRepo;
            this._cataloguesRepo = cataloguesRepo;
            this._filestoreRepo = filestoreRepo;
            this._productownerRepo = productownerRepo;
        }
        public IList<Product> GetProducts()
        {            
            var list = from p in _productRepo.Table join f in _filestoreRepo.Table 
                       on p.ProductId equals f.FK_FileStore_ProductId                           
                       into f1 from f2 in f1.DefaultIfEmpty()                                              
                       select p;                        
            return list.ToList();
        }
        public IList<Product> GetProductsByProductOwnerID(long ProductOwnerId)        
        {
            //Many to Many relationship            
            var list = from p in _productRepo.Table
                       where p.ProductOwner.ProductOwnerId == ProductOwnerId
                       select p;

            return list.ToList();
        }
        public Product GetOneProductWithMoreInfo(long ProductId)
        {
            var list = from p in _productRepo.Table
                       where p.ProductId == ProductId
                       select p;
           
            return list.FirstOrDefault<Product>();
        }

        public IList<Product> GetProductsByCataloguesID(long CataloguesId)
        {                        
            var list = from p in _productRepo.Table
                       join c in _cataloguesRepo.Table
                           on p.FK_Product_CataloguesId equals c.CataloguesId    
                           where p.FK_Product_CataloguesId == CataloguesId                   
                       select p;                       
            return list.ToList();
        }
       
        public Product GetProduct(long ProductId)
        {
            var p = _productRepo.GetById(ProductId);
            return p;
        }

        public void Insert(Product product)
        {
            _productRepo.Insert(product);
        }
        public void Delete(Product product)
        {
            _productRepo.Delete(product);
        }
        public void Update(Product product)
        {
            _productRepo.Update(product);            
        }
    }
}