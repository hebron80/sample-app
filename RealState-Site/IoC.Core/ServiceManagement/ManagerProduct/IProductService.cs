﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IoC.Core.Entities;

namespace IoC.Core.ServiceManagement.ManagerProduct
{
    public interface IProductService:IService<Product>
    {
        IList<Product> GetProducts();
        IList<Product> GetProductsByProductOwnerID(long ProductOwnerId);        
        Product GetOneProductWithMoreInfo(long ProductId);
        IList<Product> GetProductsByCataloguesID(long CataloguesId);
        Product GetProduct(long Id);
        
    }
}
