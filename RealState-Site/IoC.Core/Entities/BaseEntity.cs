﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IoC.Core.Entities
{
    public abstract class BaseEntity
    {
        public Int64 Id { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateCreated { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateUpdated { get; set; }
    }
}