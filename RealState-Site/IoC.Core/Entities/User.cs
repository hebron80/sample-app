﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace IoC.Core.Entities
{
    public class User:Person
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        public string AccountName { get; set; }
        public string Password { get; set; }
        //private string Granted { get; set; } // Dependence on the role of User that they have difference role

        public bool Activated { get; set; }
        public string Description { get; set; }      
        public int FK_RoleId { get; set; }
        //public virtual Role Role { get; set; }


    }
}
