﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IoC.Core.Entities
{
    public abstract class Person:BaseEntity
    {
    
        //[Key]
        //public int Id { get; set; }


        [Required]
        [StringLength(50, ErrorMessage="First Name cannot be longer than 50 characters.")]
        [Column("FirstName")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }


        [Required]
        [StringLength(50)]
        [Display(Name="Last Name")]
        public string LastName { get; set; }


        [Display(Name="Full Name"  )]
        public string FullName 
        {
            get 
            {
                return LastName + " " + FirstName;
            }
        }        
    }
}
