﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IoC.Core.Entities
{
    public class Catalogues:BaseEntity
    {       
        [Key]      
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 CataloguesId { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }  
    

        public virtual ICollection<Product> Products { get; set; }
    }
}
