﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IoC.Core.Entities
{
    public class SlideHome:BaseEntity
    {
        [Key]
        public Int64 ImageId { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
    }
}