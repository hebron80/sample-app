﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IoC.Core.Entities
{
    public class Product:BaseEntity
    {        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 ProductId { get; set; }


        //[Required]
        [StringLength(100,ErrorMessage="The Name is mandatory")]
        [Display(Name="Name of production")]
        public string Name { get; set; }


        //[Required]        
        [Display(Name = "Hight of production")]
        public float Hight { get; set; }


        //[Required]        
        [Display(Name = "Width of production")]
        public float Width { get; set; }

        public decimal Price { get; set; }

        //[Required]        
        [Display(Name = "Direction of production")]
        public string Direction { get; set; } //East, West, South, North of the Production


        //[Required]        
        [Display(Name = "the Floor number of the house")]
        public int Floors { get; set; }

        //[Required]
        [Display(Name = "the Status of the production")]
        public string Status { get; set; } //So hong, so do, giay tay

        //[Required]
        [Display(Name = "the WC number of the house")]
        public int WC { get; set; }

        //[Required]
        [Display(Name = "the BedRoom number of the house")]
        public int BedRoom { get; set; }

        //[Required]
        [Display(Name = "the BathRoom number of the house")]
        public int BathRoom { get; set; }

        //[Required]
        [StringLength(500,ErrorMessage="The Environment Around House is mandatory")]
        [Display(Name = "the Environment around the house")]
        public string EAH { get; set; } //Environment Around House         


        //[Required]
        [StringLength(500, ErrorMessage = "The Description is mandatory")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public string Address { get; set; }
        public string Ward { get; set; }
        public string District { get; set; }
        public string LocatedMap { get; set; }
        public string Video1 { get; set; }
        public string Video2 { get; set; }
        public string Video3 { get; set; }

        public Int64 FK_Product_CataloguesId { get; set; }
        public Int64 FK_Product_ProductOwnerId { get; set; }     
  

        public virtual Catalogues Catalogues { get; set; }
        //public virtual ICollection<ProductOwner> ProductOwners { get; set; }
        public virtual ProductOwner ProductOwner { get; set; }
        public virtual ICollection<FileStore> FileStores { get; set; }
    }
}
