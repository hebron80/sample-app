﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace IoC.Core.Entities
{
    public class FileStore:BaseEntity
    {
        [Key]
        public Int64 FileId { get; set; }
        public string FileName { get; set; }                
        public string Description { get; set; }
        public Int64 FK_FileStore_ProductId { get; set; }
        public virtual Product Product { get; set; }

    }
}